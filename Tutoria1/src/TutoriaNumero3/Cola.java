package TutoriaNumero3;

public class Cola {

    Nodo inicioCola;
    Nodo finCola;
    int cantidadElementos;

    public Cola() {
        inicioCola = null;
        finCola = null;
        cantidadElementos = 0;
    }

    public boolean vacia() {
        return inicioCola == null;
    }

    public void insertarEnCola(String elemento) {
        Nodo nuevoNodo = new Nodo(elemento);
        if (vacia()) {
            inicioCola = nuevoNodo;
        } else {
            finCola.siguiente = nuevoNodo;

        }
        finCola = nuevoNodo;
        cantidadElementos++;
    }

    public String atender() {

        String persona = inicioCola.dato;
        inicioCola = inicioCola.siguiente;
        cantidadElementos--;
        return persona;
    }

    public String inicioCola() {
        return inicioCola.dato;
    }

    public int tamañoCola() {
        return cantidadElementos;
    }
}
