package TutoriaNumero3;

public class Lista {

    private Nodo inicio;
    private Nodo fin;
    
    public Lista() {
        inicio = null;
        fin = null;
    }

    public void agregarInicio(String dato) {
        inicio = new Nodo(dato, inicio);
        if (fin == null) {
            fin = inicio;
        }
    }

    public StringBuilder mostrarLista() {
        Nodo recorrer = inicio;
        StringBuilder lista = new StringBuilder();
        while (recorrer != null) {
            lista.append("\n").append(recorrer.dato);
            recorrer = recorrer.siguiente;
        }
        return lista;
    }

    public boolean listaVacia() {
        if (inicio == null) {
            return true;
        } else {
            return false;
        }
    }

    public void agregaFinal(String dato) {
        if (listaVacia()) {
            inicio = fin = new Nodo(dato);
        } else {
            fin.siguiente = new Nodo(dato);
            fin = fin.siguiente;
        }
    }

    public String eliminarInicio() {
        String borrado = inicio.dato;
        if (inicio == fin) {
            inicio = fin = null;
        } else {
            inicio = inicio.siguiente;
        }
        return borrado;
    }
    public String eleiminarFinal()
    {
        String borrado = fin.dato;
        if(inicio==fin)
        {
            inicio=fin=null;
        }else
        {
            Nodo temporal=inicio;
            while(temporal.siguiente!=fin)
            {
                temporal=temporal.siguiente;
            }
            fin=temporal;
            fin.siguiente=null;
        }
        return borrado;
    }
    public void eliminar(String dato)
    {
        if(listaVacia())
        {
            
        }else
        {
            if(inicio==fin && dato==inicio.dato)
            {
                inicio=fin=null;
            }else if(dato==inicio.dato)
            {
                inicio=inicio.siguiente;
            }else
            {
                Nodo anterior=inicio;
                Nodo temporal=inicio.siguiente;
                while(temporal!=null&&temporal.dato!=dato)
                {
                    anterior=anterior.siguiente;
                    temporal=temporal.siguiente;
                }
                if(temporal!=null)
                {
                    anterior.siguiente=temporal.siguiente;
                    if(temporal==fin)
                    {
                        fin=anterior;
                    }
                }
            }
        }
    }
    public boolean buscar(String dato)
    {
        Nodo temporal=inicio;
        while(temporal!=null&&temporal.dato!=dato)
        {
            temporal=temporal.siguiente;
        }
        return temporal!=null;
    }
}
