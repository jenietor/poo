
package TutoriaNumero3;


public class Nodo {
     String dato;
     Nodo siguiente;
    
    public Nodo(String dato )
    {
        this.dato=dato;
        this.siguiente=null;
    }
    
    public Nodo(String dato, Nodo nodo)
    {
        this.dato=dato;
        siguiente=nodo;
    }
}
