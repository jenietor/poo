package TutoriaNumero3;

import javax.swing.JOptionPane;

public class Principal {

    public static void main(String[] args) {
        Cola nuevaCola = new Cola();
        Lista nuevaista = new Lista();
        Pila nuevaPila = new Pila();
        String opcion;
        String persona;
        boolean respuesta;
        do {
            opcion = JOptionPane.showInputDialog("Para ingresar al menu de cola ingrese: 1.\n"
                    + "Para ingresar al menu de lista ingrese: 2.\nPara ingresar al menu de pila ingrese: 3\n."
                    + "Para salir ingrese la palabra: salir");
            switch (opcion) {
                case ("1"):
                    do {
                        opcion = JOptionPane.showInputDialog("Para insertar una persona en la cola ingrese: 1.\n"
                                + "Para atender la cola ingrese:2.\nPara comprobar la persona que se encuentra al inicio de la cola "
                                + "ingrese: 3.\nPara ver el tamaño de la cola ingrese: 4\n"
                                + "Para volver al menu principal ingrese: 0.");
                        switch (opcion) {
                            case ("1"):
                                opcion = JOptionPane.showInputDialog("Ingrese el nombre que va insertar en la cola");
                                nuevaCola.insertarEnCola(opcion);
                                break;
                            case ("2"):
                                if (nuevaCola.vacia()) {
                                    JOptionPane.showMessageDialog(null, "La cola esta vacia");
                                } else {
                                    JOptionPane.showMessageDialog(null, "Se atendio el señor(a): " + nuevaCola.atender());
                                }
                                break;
                            case ("3"):
                                if (nuevaCola.vacia()) {
                                    JOptionPane.showMessageDialog(null, "La cola esta vacia");
                                } else {
                                    JOptionPane.showMessageDialog(null, "La siguiente persona por atender es: " + nuevaCola.inicioCola());
                                }
                                break;
                            case ("4"):
                                JOptionPane.showMessageDialog(null, "Faltan " + nuevaCola.tamañoCola() + " personas por atender");
                                break;
                            case ("0"):
                                JOptionPane.showMessageDialog(null, "Menu Principal");
                                break;
                            default:
                                JOptionPane.showMessageDialog(null, "La opcion que se ha ingresado aún no asido implementada, INGRESE UNA OPCION VALIDA");
                                break;
                        }
                    } while (!opcion.equals("0"));
                    break;
                case ("2"):
                    do {
                        opcion = JOptionPane.showInputDialog("Para insertar un valor al inicio de la lista ingrese: 1.\n"
                                + "Para ver los valores de la lista ingrese: 2.\nPara ingresar un valor al final de la lista ingrese: 3.\n"
                                + "Para eliminar el valor del inicio de la lista ingrese: 4.\nPara eliminar el valor del final de la lista ingrese: 5\n"
                                + "Para eliminar un elemento de la lista ingrese:6.\nPara buscar un elemento en la lista ingresa:7\n"
                                + "Para volver al menu principal ingrese: 0.");
                        switch (opcion) {
                            case ("1"):
                                opcion = JOptionPane.showInputDialog("Ingresa el valor que deseas insertar al inicio de la lista");
                                nuevaista.agregarInicio(opcion);
                                break;
                            case ("2"):
                                JOptionPane.showMessageDialog(null, nuevaista.mostrarLista());
                                break;
                            case ("3"):
                                opcion = JOptionPane.showInputDialog("Ingresa el valor que deseas insertar al final de la lista");
                                nuevaista.agregaFinal(opcion);
                                break;
                            case ("4"):
                                if (nuevaista.listaVacia()) {
                                    JOptionPane.showMessageDialog(null, "La lista esta vacia");
                                } else {
                                    JOptionPane.showMessageDialog(null, "El elemento que se elimino fue: " + nuevaista.eliminarInicio());
                                }
                                break;
                            case ("5"):
                                if (nuevaista.listaVacia()) {
                                    JOptionPane.showMessageDialog(null, "La lista esta vacia");
                                } else {
                                    JOptionPane.showMessageDialog(null, "El elemento que se elimino fue: " + nuevaista.eleiminarFinal());
                                }
                                break;
                            case ("6"):
                                opcion = JOptionPane.showInputDialog("Ingrese el dato que desea eliminar");
                                if (nuevaista.buscar(opcion)) {
                                    nuevaista.eliminar(opcion);
                                } else {
                                    JOptionPane.showMessageDialog(null, "El dato No se encuentra en la lista");
                                }
                                break;
                            case ("7"):
                                opcion = JOptionPane.showInputDialog("Ingrese el dato que desea buscar");
                                if (nuevaista.buscar(opcion)) {
                                    JOptionPane.showMessageDialog(null, "El dato Si se encuentra en la lista");
                                } else {
                                    JOptionPane.showMessageDialog(null, "El dato No se encuentra en la lista");
                                }
                                break;
                            case ("0"):
                                JOptionPane.showMessageDialog(null, "Menu Principal");
                                break;
                            default:
                                JOptionPane.showMessageDialog(null, "La opcion que se ha ingresado aún no asido implementada, INGRESE UNA OPCION VALIDA");
                                break;
                        }
                    } while (!opcion.equals("0"));
                    break;
                case ("3"):
                    do {
                        opcion = JOptionPane.showInputDialog("Para insertar un valor en la pila ingrese: 1.\n"
                                + "Para sacar un valor de la pila ingrese: 2.\nPara ver el tamaño de la pila ingrese: 3.\n"
                                + "Para vaciar la pila ingrese: 4.\nPara comprobar el elemento de la pila que se encuentra en la cima ingrese: 5\n"
                                + "Para volver al menu principal ingrese: 0.");
                        switch (opcion) {
                            case ("1"):
                                opcion = JOptionPane.showInputDialog("Ingresa el valor que deseas insertar en la pila");
                                nuevaPila.insertar(opcion);
                                break;
                            case ("2"):
                                if (nuevaPila.pilaVacia()) {
                                    JOptionPane.showMessageDialog(null, "La pila se encuentra vacia ");
                                } else {
                                    JOptionPane.showMessageDialog(null, "El valor que se saco de la pila fue: " + nuevaPila.sacar());
                                }
                                break;
                            case ("3"):
                                opcion = JOptionPane.showInputDialog("El tamaño de la pila es: " + nuevaPila.tamañoPila());
                                break;
                            case ("4"):
                                if (nuevaPila.pilaVacia()) {
                                    JOptionPane.showMessageDialog(null, "La pila se encuentra vacia ");
                                } else {
                                    nuevaPila.vaciarPila();
                                    JOptionPane.showMessageDialog(null, "La pila ya fue vaciada");
                                }
                                break;
                            case ("5"):
                                if (nuevaPila.pilaVacia()) {
                                    JOptionPane.showMessageDialog(null, "La pila se encuentra vacia ");
                                } else {

                                    JOptionPane.showMessageDialog(null, "El valor que se encuentra en la cima de la pila es: " + nuevaPila.cima());
                                }
                                break;
                            case ("0"):
                                JOptionPane.showMessageDialog(null, "Menu Principal");
                                break;
                            default:
                                JOptionPane.showMessageDialog(null, "La opcion que se ha ingresado aún no asido implementada, INGRESE UNA OPCION VALIDA");
                                break;
                        }
                    } while (!opcion.equals("0"));
                    break;

                case ("salir"):
                    JOptionPane.showMessageDialog(null, "HASTA PRONTO");
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "La opcion que se ha ingresado aún no asido implementada, INGRESE UNA OPCION VALIDA");
                    break;
            }

        } while (!opcion.equals("salir"));
    }
}
