package TutoriaNumero3;

public class Pila {

    private Nodo cima;
    private int tamañoPila;

    public Pila() {
        cima = null;
        tamañoPila = 0;
    }

    public boolean pilaVacia() {
        return cima == null;
    }

    public void insertar(String dato) {
        Nodo nuevoNodo = new Nodo(dato);
        nuevoNodo.siguiente = cima;
        cima = nuevoNodo;
        tamañoPila++;
    }
    public String sacar()
    {
        String sale=cima.dato;
        cima=cima.siguiente;
        tamañoPila--;
        return sale;
    }
    public String cima()
    {
        return cima.dato;
    }
    public int tamañoPila()
    {
        return tamañoPila;
    }
    public void vaciarPila()
    {
        while(!pilaVacia())
        {
            sacar();
        }
    }
}
