package tutoria1;

import java.util.Collections;

import java.util.Stack;

public class Pila {

    private Stack pila;
    private boolean ban;
    private int tamaño;

    public Pila() {
        pila = new Stack<>();
    }

    public void adicionarApila(String valor) {
        pila.push(valor);
    }

    public void mostarPila() {
        if (pila.empty()) {
            System.out.println("La pila se encuentra vacia");
        } else {
            for (int i = 0; i < pila.size(); i++) {
                System.out.println("Posicion " + i + " :" + pila.get(i));
            }
        }

    }

    public void eliminarPila() {
        if (pila.empty()) {
            System.out.println("La pila se encuentra vacia");
        } else {
            while (!pila.empty()) {
                pila.pop();
            }
            System.out.println("La pila fue vaciada satisfactoriamente");
        }
    }

    public void ordenarPilaAscendente() {
        Collections.sort(pila);
        mostarPila();
    }

    public void ordenarPilaDescendente() {
        Collections.sort(pila, Collections.reverseOrder());
        mostarPila();
    }

    public boolean buscarEnPila(String registro) {
        ban = pila.contains(registro);
        return ban;
    }

    public void eliminarRegistroPila(String registro) {
        if (buscarEnPila(registro)) {
            pila.remove(registro);
            System.out.println("Registro eliminado satisfactoriamente");
        } else {
            System.out.println("Registro no encontrado en la pila");
        }
    }

    public void cargarautomaticamentePila() {
        pila.add("Enero");
        pila.add("Febrero");
        pila.add("Marzo");
        pila.add("Abril");
        pila.add("Mayo");
        pila.add("Junio");
        pila.add("Julio");
        pila.add("Agosto");
        pila.add("Septiembre");
        pila.add("Octubre");
        pila.add("Noviembre");
        pila.add("Diciembre");

    }

    public int tamañoPila() {
        tamaño = pila.size();
        return tamaño;
    }
}
