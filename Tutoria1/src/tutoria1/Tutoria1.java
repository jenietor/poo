package tutoria1;

import java.util.Scanner;

public class Tutoria1 {

    public static void main(String[] args) {

        int[] arreglo = new int[10];
        Lista nuevaLista = new Lista();
        Pila nuevaPila = new Pila();
        Cola nuevaCola = new Cola();
        String nombre = "";
        ArregloLineal nuevo = new ArregloLineal();
        ArregloBidimensional nuevaMatriz = new ArregloBidimensional();
        Scanner entr = new Scanner(System.in);
        String opc;
        int numero = 0;

        nuevaLista.mostrarLista();
        do {
            System.out.println("\n************************************MENU PRINCIPAL****************************************");
            System.out.println("*   OPCIONES:                                                                  *");
            System.out.println("Para ingresar al menu de ARREGLOS UNIDIMENSIONALES digite 1 ");
            System.out.println("Para ingresar al menu de MATRICES digite 2 ");
            System.out.println("Para ingresar al menu de LISTAS digite 3 ");
            System.out.println("Para ingresar al menu de PILAS digite 4 ");
            System.out.println("Para ingresar al menu de COLAS digite 5 ");
            System.out.println("Para salir del programa digite s");
            opc = entr.next();
            switch (opc) {
                case ("1"):
                    do {

                        System.out.println("*************************MENU DE ARREGLO********************************");
                        System.out.println("*   Digite: 1 Para Cargar el arreglo unidimencional Manualmente                *");
                        System.out.println("*   Digite: 2 Para Cargar el arreglo unidimencional Automaticamente            *");
                        System.out.println("*   Digite: 3 Para Mostrar el arreglo unidimencional                           *");
                        System.out.println("*   Digite: 4 Para Ordenar el arreglo unidimencional de manera Ascendente      *");
                        System.out.println("*   Digite: 5 Para Ordenar el arreglo unidimencional Descendentemente          *");
                        System.out.println("*   Digite: 6 Para Buscar un número en el arreglo unidimencional               *");
                        System.out.println("*   Digite: 0 Para volver al menu principal                                     ");
                        opc = entr.next();
                        switch (opc) {
                            case ("1"):
                                System.out.println("Digite los 10 números del arreglo unidimensional: ");
                                for (int i = 0; i < arreglo.length; i++) {
                                    arreglo[i] = entr.nextInt();
                                }
                                nuevo = new ArregloLineal(arreglo);
                                break;
                            case ("2"):
                                nuevo.cargarAuto();
                                break;
                            case ("3"):
                                nuevo.mostrar();
                                break;
                            case ("4"):
                                nuevo.ordenarAscendente();
                                break;
                            case ("5"):
                                nuevo.ordenarDescendente();
                                break;
                            case ("6"):
                                System.out.println("Digite el número que desea buscar dentro del arreglo unidimensional: ");
                                numero = entr.nextInt();
                                if (nuevo.buscar(numero)) {
                                    System.out.println("El número: " + numero + " Si se encuentra dentro del arreglo");
                                } else {
                                    System.out.println("El número: " + numero + " No se encuentra dentro del arreglo");
                                }
                                break;
                            case ("0"):
                                break;
                            default:
                                System.out.println("La opcion que ha seleccionado no esta disponible");
                                break;
                        }
                    } while (!opc.equals("0"));
                    break;
                case ("2"):
                    do {
                        System.out.println("***************************MENU DE MATRICES*************************************");
                        System.out.println("*   Digite: 1 Para Cargar la matriz 1                                          *");
                        System.out.println("*   Digite: 2 Para Cargar la matriz 2                                          *");
                        System.out.println("*   Digite: 3 Para Mostrar las Matrices                                        *");
                        System.out.println("*   Digite: 4 Para Sumar las Matrices                                         *");
                        System.out.println("*   Digite: 5 Para Resta las Matrices                                         *");
                        System.out.println("*   Digite: 0 Para volver al menu principal");
                        opc = entr.next();
                        switch (opc) {

                            case ("1"):
                                nuevaMatriz.cargarMatriz1();
                                break;
                            case ("2"):
                                nuevaMatriz.cargarMatriz2();
                                break;
                            case ("3"):
                                nuevaMatriz.mostrarMatrizes();
                                break;
                            case ("4"):
                                nuevaMatriz.sumaMatrices();
                                break;
                            case ("5"):
                                nuevaMatriz.restaMatrices();
                                break;
                            case ("0"):
                                break;
                            default:
                                System.out.println("La opcion que ha seleccionado no esta disponible");
                                break;
                        }

                    } while (!opc.equals("0"));
                    break;
                case ("3"):
                    do {
                        System.out.println("******************************MENU DE LISTA****************************");
                        System.out.println("Para adicionar un nombre nuevo en la lista ingrese 1");
                        System.out.println("Para ver los valores de la lista ingrese 2");
                        System.out.println("Para ordenar la lista de manera Ascendente ingrese 3");
                        System.out.println("Para ordenar la lista de manera Descendente ingrese 4");
                        System.out.println("Para Eliminar un registro de la lista ingrese 5");
                        System.out.println("Para ver el tamaño de la lista ingrese 6");
                        System.out.println("Para cargar los días de la semana ingrese 7");
                        System.out.println("Para limpiar la lista ingrese 8");
                        System.out.println("Para buscar un registro dentro de la lista ingrese 9");
                        System.out.println("Para volver al menu anterior ingrese 0");
                        opc = entr.next();
                        switch (opc) {
                            case ("1"):
                                System.out.println("Ingrese un registro");
                                nombre = entr.next();
                                nuevaLista.adicionar(nombre);
                                System.out.println("El registro fue ingresado Satisfactoriamente :)");
                                break;
                            case ("2"):
                                nuevaLista.mostrarLista();
                                break;
                            case ("3"):
                                nuevaLista.ordenarListaAscendente();
                                break;
                            case ("4"):
                                nuevaLista.ordenarListaDescendente();
                                break;
                            case ("5"):
                                System.out.println("Ingresa el registro que deseas eliminar de la lista");
                                nombre = entr.next();
                                nuevaLista.eliminarRegistro(nombre);
                                break;
                            case ("6"):
                                System.out.println("El tamaño de la lista es :" + nuevaLista.tamanioLista());
                                break;
                            case ("7"):
                                nuevaLista.cargarAutomaticamente();
                                break;
                            case ("8"):
                                nuevaLista.limpiarLista();
                                break;
                            case ("9"):
                                System.out.println("Ingrese el registro que desea buscar");
                                nombre = entr.next();
                                if (nuevaLista.buscarEnLista(nombre)) {
                                    System.out.println("El registro se encuentra en la lista actual");
                                } else {
                                    System.out.println("El registro no se encuentra en la lista actual");
                                }
                                break;
                            case ("0"):
                                System.out.println("Hasta Pronto");
                                break;
                            default:
                                System.out.println("Opcion invalida verifica el menu y eleige una opcion valida");
                                break;
                        }

                    } while (!opc.equals("0"));
                    break;
                case ("4"):
                    do {
                        System.out.println("**************************MENU DE PILA***************************************");
                        System.out.println("Para adicionar un nombre nuevo en la pila ingrese 1");
                        System.out.println("Para ver los valores de la pila ingrese 2");
                        System.out.println("Para vaciar la pila ingrese 3");
                        System.out.println("Para ordenar la pila de manera Ascendente ingrese 4");
                        System.out.println("Para ordenar la pila de manera Descendente ingrese 5");
                        System.out.println("Para buscar un registro en la pila ingresa 6");
                        System.out.println("Para eliminar un registro dentro de la pila ingrese 7");
                        System.out.println("Para cargar la pila con los meses del año ingrese 8");
                        System.out.println("Para ver el tamaño de la pila ingrese 9");
                        System.out.println("Para volver al menu anterior ingrese 0");
                        opc = entr.next();
                        switch (opc) {
                            case ("1"):
                                System.out.println("Ingrese un registro");
                                nombre = entr.next();
                                nuevaPila.adicionarApila(nombre);
                                System.out.println("El registro fue ingresado Satisfactoriamente :)");
                                break;
                            case ("2"):
                                nuevaPila.mostarPila();
                                break;
                            case ("3"):
                                nuevaPila.eliminarPila();
                                break;
                            case ("4"):
                                nuevaPila.ordenarPilaAscendente();
                                break;
                            case ("5"):
                                nuevaPila.ordenarPilaDescendente();
                                break;
                            case ("6"):
                                System.out.println("Ingrese el registro que desea buscar");
                                nombre = entr.next();
                                if (nuevaPila.buscarEnPila(nombre)) {
                                    System.out.println("El registro Si Existe en la pila");
                                } else {
                                    System.out.println("El registro No se encuentra en la pila");
                                }
                                break;
                            case ("7"):
                                System.out.println("Ingrese el registro que desea eliminar de la pila");
                                nombre = entr.next();
                                nuevaPila.eliminarRegistroPila(nombre);
                                break;
                            case ("8"):
                                nuevaPila.cargarautomaticamentePila();
                                break;
                            case ("9"):
                                System.out.println("El tamaño de la pila es: " + nuevaPila.tamañoPila());
                                break;
                            case ("0"):
                                System.out.println("Hasta Pronto");
                                break;
                            default:
                                System.out.println("Opcion invalida verifica el menu y eleige una opcion valida");
                                break;
                        }
                    } while (!opc.equals("0"));
                    break;
                case ("5"):
                    do {
                        System.out.println("*********************MENU DE COLA**********************");
                        System.out.println("Para adicionar un nuevo registro a la cola ingrese 1");
                        System.out.println("Para ver los valores en cola ingrese 2");
                        System.out.println("Para desencolar la cola ingrese 3");
                        System.out.println("Para ordenar la cola de manera Ascendente ingrese 4");
                        System.out.println("Para ordenar la cola de manera Descendente ingrese 5");
                        System.out.println("Para buscar un registro en la cola ingresa 6");
                        System.out.println("Para eliminar un registro dentro de la cola ingrese 7");
                        System.out.println("Para cargar la cola con los integrantes del cipa ingrese 8");
                        System.out.println("Para ver el tamaño de la cola ingrese 9");
                        System.out.println("Para volver al menu anterior ingrese 0");
                        opc = entr.next();
                        switch (opc) {
                            case ("1"):
                                System.out.println("Ingrese un registro");
                                nombre = entr.next();
                                nuevaCola.adicionalACola(nombre);
                                break;
                            case ("2"):
                                nuevaCola.mostrarCola();
                                break;
                            case ("3"):
                                nuevaCola.desencolar();
                                break;
                            case ("4"):
                                nuevaCola.ordenarAscendente();
                                break;
                            case ("5"):
                                nuevaCola.ordenarDescendente();
                                break;
                            case ("6"):
                                System.out.println("Ingrese el registro que desea buscar");
                                nombre = entr.next();
                                if (nuevaCola.buscarEnCola(nombre)) {
                                    System.out.println("El registro esta en colado");
                                } else {
                                    System.out.println("El registro No se encuentra en la cola");
                                }
                                break;
                            case ("7"):
                                System.out.println("Ingrese el registro que desea desencolar");
                                nombre = entr.next();
                                nuevaCola.eliminarRegistro(nombre);
                                break;
                            case ("8"):
                                nuevaCola.cargarColaautomaticamente();
                                break;
                            case ("9"):
                                System.out.println("El tamaño de la cola es: " + nuevaCola.tamañoCola());
                                break;
                            case ("0"):
                                System.out.println("Hasta Pronto");
                                break;
                            default:
                                System.out.println("Opcion invalida verifica el menu y eleige una opcion valida");
                                break;
                        }
                    } while (!opc.equals("0"));
                    break;
                case ("s"):
                    System.out.println("HASTA PRONTO");
                    break;
                default:
                    System.out.println("Opcion invalida verifica el menu y eleige una opcion valida");
                    break;
            }

        } while (!opc.equals("s"));
    }

}
