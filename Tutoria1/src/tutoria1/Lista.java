package tutoria1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Lista {

    private List lista;
    private boolean ban;

    public Lista() {
        lista = new ArrayList<>();
    }

    public void adicionar(String valor) {

        lista.add(valor);
    }

    public void mostrarLista() {
        if (lista.isEmpty()) {
            System.out.println("La lista se encuentra vacia, por favor ingrese registros a la lista");
        } else {
            for (int i = 0; i < this.lista.size(); i++) {
                System.out.println("Psocicion " + i + " :" + lista.get(i));
            }
        }

    }

    public void ordenarListaAscendente() {
        if (lista.isEmpty()) {
            System.out.println("La lista se encuentra vacia, por favor ingrese registros a la lista");

        } else {
            Collections.sort(lista);
            mostrarLista();
        }

    }

    public void eliminarRegistro(String regis) {

        if (buscarEnLista(regis)) {
            lista.remove(regis);
            System.out.println("El registro fue eliminado satisfactoriamente");
        } else {
            System.out.println("El registro no fue encontrado");
        }
    }

    public void ordenarListaDescendente() {
        if (lista.isEmpty()) {
            System.out.println("La lista se encuentra vacia, por favor ingrese registros a la lista");
        } else {
            Collections.sort(lista, Collections.reverseOrder());
            mostrarLista();
        }

    }

    public void limpiarLista() {
        if (lista.isEmpty()) {
            System.out.println("No hay registros en la lista");
        } else {
            lista.clear();
            System.out.println("La lista fue vaciada");
        }

    }

    public int tamanioLista() {
        return lista.size();
    }

    public void cargarAutomaticamente() {
        lista.add("Lunes");
        lista.add("Martes");
        lista.add("Miercoles");
        lista.add("Jueves");
        lista.add("Viernes");
        lista.add("Sabado");
        lista.add("Domingo");
        mostrarLista();
    }

    public boolean buscarEnLista(String registro) {
        ban = lista.contains(registro);
        return ban;
    }
}
