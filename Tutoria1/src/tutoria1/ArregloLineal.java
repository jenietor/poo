package tutoria1;

public class ArregloLineal {

    private int arreglo[] = new int[10];

    public ArregloLineal() {

    }

    public ArregloLineal(int[] arreglo) {
        for (int i = 0; i < this.arreglo.length; i++) {
            this.arreglo[i] = arreglo[i];
        }
    }

    public void cargarAuto() {

        for (int i = 0; i < 10; i++) {
            arreglo[i] = (int) (Math.random() * 1000) + 1;
        }

    }

    public void mostrar() {
        System.out.print("[");
        for (int i = 0; i < 10; i++) {
            if (i < this.arreglo.length - 1) {
                System.out.print(this.arreglo[i] + ", ");
            } else {
                System.out.println(this.arreglo[i] + "]");
            }

        }
    }

    public void ordenarAscendente() {

        int temp = 0;
        for (int i = 0; i < arreglo.length; i++) {

            for (int j = i + 1; j < arreglo.length; j++) {
                if (arreglo[j] > arreglo[i]) {
                    temp = arreglo[i];
                    arreglo[i] = arreglo[j];
                    arreglo[j] = temp;
                }
            }
        }
        mostrar();

    }

    public void ordenarDescendente() {

        int temp = 0;
        for (int i = 0; i < arreglo.length; i++) {

            for (int j = i + 1; j < arreglo.length; j++) {
                if (arreglo[j] < arreglo[i]) {
                    temp = arreglo[i];
                    arreglo[i] = arreglo[j];
                    arreglo[j] = temp;
                }
            }
        }
        mostrar();
    }

    public boolean buscar(int num) {
        boolean ban = false;
        for (int i = 0; i < arreglo.length; i++) {
            if (num == arreglo[i]) {
                ban = true;
                break;
            }
        }
        return ban;
    }

}
