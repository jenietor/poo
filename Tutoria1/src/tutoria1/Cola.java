package tutoria1;

import java.util.Collections;
import java.util.LinkedList;

public class Cola {

    private LinkedList<String> cola;
    private boolean ban;
    private int tamaño;

    public Cola() {
        cola=new LinkedList<>();
    }
    public void adicionalACola(String registro)
    {
        cola.addLast(registro);
        System.out.println("Registro agregado satisfactoriamente");
    }
    public void mostrarCola()
    {
        for(int i=0;i<cola.size();i++)
        {
            System.out.print("Posición "+i+" : "+cola.get(i));
        }
    }
    public void desencolar()
    {
        cola.clear();
        System.out.println("Ya fue desencolada la cola");
    }
    public void ordenarAscendente()
    {
        Collections.sort(cola);
        mostrarCola();
    }
    public void ordenarDescendente()
    {
        Collections.sort(cola, Collections.reverseOrder());
        mostrarCola();
    }
    public boolean buscarEnCola(String registro)
    {
        ban=cola.contains(registro);
        return ban;
    }
    public void eliminarRegistro(String registro)
    {
        if(buscarEnCola(registro))
        {
            cola.remove(registro);
            System.out.println("Registro eliminado satisfactoriamente");
        }else
        {
            System.out.println("El registro no fue encontrado, intentenuevamente");
        }
    }
    public void cargarColaautomaticamente()
    {
        cola.addLast("Joan");
        cola.addLast("David");
        cola.addLast("Cristian");
        cola.addLast("Diana");
    }
    public int tamañoCola()
    {
        tamaño=cola.size();
        return tamaño;
    }
}
