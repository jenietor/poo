package tutoria1;

public class ArregloBidimensional {

    private int[][] matriz = new int[3][3];
    private int[][] matriz2 = new int[3][3];
    private int[][] matriz3 = new int[3][3];

    public void cargarMatriz1() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                matriz[i][j] = (int) (Math.random() * 1000) + 1;
            }
        }
    }
    public void cargarMatriz2() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                matriz2[i][j] = (int) (Math.random() * 1000) + 1;
            }
        }
    }
    public void mostrarMatrizes()
    {
        System.out.println("*******MATRIZ1***********");
        for (int i = 0; i < 3; i++) {
            for(int j=0;j<3;j++)
            {
                System.out.print(matriz[i][j]);
                if(j<=2)System.out.print(", ");
            }
            System.out.println("\n");
        }
        System.out.println("*******MATRIZ2***********");
        for (int i = 0; i < 3; i++) {
            for(int j=0;j<3;j++)
            {
                System.out.print(matriz2[i][j]+", ");
                
            }
            System.out.println("\n");
        }
        
    }
    public void sumaMatrices()
    {
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                matriz3[i][j]=matriz2[i][j]+matriz[i][j];
                System.out.print(matriz3[i][j]+", ");
            }
            System.out.println("\n");
        }
    }
    
    public void restaMatrices()
    {
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                matriz3[i][j]=matriz2[i][j]-matriz[i][j];
                System.out.print(matriz3[i][j]+", ");
            }
            System.out.println("\n");
        }
    }
}
